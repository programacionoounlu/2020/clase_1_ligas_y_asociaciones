import java.util.ArrayList;

public class Clase {
	private final int codigoAsignatura;
	//private Estudiante estudiantes[];
	private ArrayList<Estudiante> estudiantes;
	//private int cantEstudiantes;
	
	public Clase(int code) {
		codigoAsignatura = code;
		//cantEstudiantes = 0;
		//estudiantes = new Estudiante[40];
		estudiantes = new ArrayList<Estudiante>();
	}

	/*
	public void setCodigoAsignatura(int code) {
		codigoAsignatura = code;
	}
	*/
	
	public void addEstudiante(Estudiante e) {
		//estudiantes[cantEstudiantes++] = e;
		//cantEstudiantes = cantEstudiantes + 1; 
		estudiantes.add(e);
	}
	
	public int getCantidadEstudiantes() {
		return estudiantes.size();
		//return cantEstudiantes;
	}
 }
