import java.util.Scanner;

public class TestEstudiante {

	public static void main(String[] args) {
		Scanner sc;
		
		// TODO Auto-generated method stub
		int a;
		int z;
		Integer b = new Integer(3);
		Integer c = new Integer(3);
		
		a = 3;
		z = 3;
		if (a == z) {
			System.out.println("Iguales");
		}
		if (a == b) {
			System.out.println("Iguales");
		}
		if (b.intValue() == c.intValue()) {
			System.out.println("Iguales?");
		}
		else
			System.out.println("Ooops");
		if (b.equals(c)) {
			System.out.println("Iguales?");
		}
		else
			System.out.println("Ooops");
		
		Estudiante e;
		e =  new Estudiante();
		e.setNombre("Andres");
		System.out.println("El nombre es " + e.getNombre());
		
		Clase poo = new Clase(11076);
		//poo.setCodigoAsignatura(11076);
		poo.addEstudiante(e);
		System.out.println("Cantidad inscriptos: " + poo.getCantidadEstudiantes());
	}

}
