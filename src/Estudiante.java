
public class Estudiante {
	private String apellido;
	private String nombre;
	private long dni;
	private int legajo;
	//private Integer legajo;

	public Estudiante(long dni) {
		this.dni = dni;
	}
	
	public Estudiante() {
		
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public long getDni() {
		return dni;
	}
	public void setDni(long dni) {
		this.dni = dni;
	}
	public int getLegajo() {
		return legajo;
	}
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

}
